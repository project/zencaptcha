CONTENTS OF THIS FILE
---------------------


 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The ZENCAPTCHA module uses the ZENCAPTCHA web service. ZENCAPTCHA protects Website forms from spam and from disposable and invalid email addresses.

 * For more information about ZENCAPTCHA and all it's features, please visit: 
   https://www.zencaptcha.com

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/zencaptcha

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/zencaptcha


REQUIREMENTS
------------

This module requires the following module outside of Drupal core:

 * [Captcha](https://www.drupal.org/project/captcha)


INSTALLATION
------------

 * Install the ZENCAPTCHA module as you would normally install a contributed Drupal module.
   Visit [Installing Modules](https://www.drupal.org/node/1897420) to learn more about installing modules on Drupal.
* Install also the [Captcha](https://www.drupal.org/project/captcha) module! (ZENCAPTCHA depends on it and the ZENCAPTCHA settings is within the CAPTCHA settings page)

CONFIGURATION
-------------

1. Go to the "Extend" section in the Administration panel and activate the module.
* Administration > Extend
    
2. Access "Configuration" in the Administration panel, then proceed to "People" and choose "CAPTCHA settings." Click on "ZENCAPTCHA."
* Administration > Configuration > People > CAPTCHA settings > ZENCAPTCHA
    
3. Enter the site and secret keys into the ZENCAPTCHA settings.

4. In the "CAPTCHA settings" under "People" in the Administration panel, specify the locations where you wish to display the ZENCAPTCHA form.
* Administration > Configuration > People > CAPTCHA settings
    
  
