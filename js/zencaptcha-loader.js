(function () {
  "use strict";
  const initializeZenCaptcha = {
    attach: function (context) {

      if(window.zenCaptchaWidget){
        const zenDomElements = context.querySelectorAll(".zenc-captcha");
        zenDomElements.forEach(function (zenDomElement) {
          if(!zenDomElement.hasChildNodes()){
            const zenWidget = new window.zenCaptchaWidget.WidgetInstance(zenDomElement);
            zenWidget.reset();
          }
        });
      }
    }
  };
  Drupal.behaviors.initZencaptcha = initializeZenCaptcha;
})();
