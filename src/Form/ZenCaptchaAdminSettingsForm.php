<?php

namespace Drupal\zencaptcha\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Zencaptcha settings for this site.
 */
class ZenCaptchaAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zencaptcha_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['zencaptcha.settings'];
  }

  public function tzen($key)
    {
      $globLang = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
      $translations = [
        'How to start the verification.' => [
            'en' => 'How to start the verification',
            'de' => 'Wie die Überprüfung gestartet werden soll',
            'fr' => 'Comment démarrer la vérification'
        ],
        'When the user clicks a field of the form.' => [
            'en' => 'When the user clicks a field of the form',
            'de' => 'Wenn der Benutzer auf ein Feld des Formulars klickt',
            'fr' => 'Quand l\'utilisateur clique sur un champ du formulaire'
        ],
        'When the user clicks on the Zencaptcha widget (recommended).' => [
            'en' => 'When the user clicks on the Zencaptcha widget (recommended)',
            'de' => 'Wenn der Benutzer auf das Zencaptcha-Widget klickt (empfohlen)',
            'fr' => 'Lorsque l\'utilisateur clique sur le widget Zencaptcha (recommandé)'
        ],
        'Verify Email Adresses (Block invalid and disposable email addresses for a cleaner user base.' => [
            'en' => 'Verify Email Adresses automatically (Block invalid and disposable email addresses for a cleaner user base)',
            'de' => 'Überprüfung von E-Mail-Adressen (Blockieren Sie automatisch ungültige und Wegwerf-E-Mail-Adressen für eine sauberere Benutzerbasis)',
            'fr' => 'Vérifier les adresses e-mail automatiquement (Bloquer les adresses e-mail invalides et jetables pour une base d\'utilisateurs plus propre)'
        ],
        'Should emails be verified (ZENCAPTCHA premium feature)' => [
            'en' => 'Should emails be verified (ZENCAPTCHA premium feature)',
            'de' => 'Sollen E-Mails überprüft werden (ZENCAPTCHA Premium-Funktion)',
            'fr' => 'Les e-mails doivent-ils être vérifiés (fonction premium de ZENCAPTCHA)'
        ],
        'The site key for your website that is generated when you register' => [
            'en' => 'The site key for your website that is generated when you register',
            'de' => 'Der Site-Schlüssel (Sitekey) für Ihre Website, der bei der Registrierung generiert wird',
            'fr' => 'La clé de site pour votre site Web qui est générée lors de votre inscription'
        ],
        'The secret key for your website that is generated when you register' => [
            'en' => 'The secret key for your website that is generated when you register',
            'de' => 'Der geheime Schlüssel (Secret Key) für Ihre Website, der bei der Registrierung generiert wird',
            'fr' => 'La clé secrète pour votre site Web qui est générée lors de votre inscription'
        ]
    ];
    

        if (array_key_exists($key, $translations)) {
            $translation = $translations[$key];
            if (isset($translation[$globLang])) {
                return $translation[$globLang];
            } else {
                return $translation['en'];
            }
        } else {
            return $key;
        }
    }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zencaptcha.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => true,
    ];

    $form['general']['zencaptcha_site_key'] = [
      '#default_value' => $config->get('site_key'),
      '#description' => $this->tzen('The site key for your website that is generated when you register').': <a href="https://www.zencaptcha.com/?ref=drupal">Zencaptcha.com</a>',
      '#maxlength' => 40,
      '#required' => true,
      '#title' => $this->t('Site key'). "<small> (Sitekey)</small>",
      '#type' => 'textfield',
    ];

    $form['general']['zencaptcha_secret_key'] = [
      '#default_value' => $config->get('secret_key'),
      '#description' => $this->tzen('The secret key for your website that is generated when you register').': <a href="https://www.zencaptcha.com/?ref=drupal">Zencaptcha.com</a>',
      '#maxlength' => 70,
      '#required' => true,
      '#title' => $this->t('Secret key'),
      '#type' => 'textfield',
    ];

    $form['general']['verification_method'] = [
      '#type' => 'radios',
      '#title' => $this->tzen('How to start the verification.'),
      '#options' => [
          'focus' => $this->tzen('When the user clicks a field of the form.'),
          'none' => $this->tzen('When the user clicks on the Zencaptcha widget (recommended).'),
      ],
      '#default_value' => $config->get('verification_method', 'none'),
    ];

    $form['general']['verify_emails'] = [
      '#type' => 'radios',
      '#description' => $this->tzen('Verify Email Adresses (Block invalid and disposable email addresses for a cleaner user base.'),
      '#title' => $this->tzen('Should emails be verified (ZENCAPTCHA premium feature)'),
      '#options' => [
          'yes' => $this->t('Yes'),
          'no' => $this->t('No'),
      ],
      '#default_value' => $config->get('verify_emails', 'no'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('zencaptcha.settings');
    $config
      ->set('site_key', $form_state->getValue('zencaptcha_site_key'))
      ->set('secret_key', $form_state->getValue('zencaptcha_secret_key'))
      ->set('verification_method', $form_state->getValue('verification_method'))
      ->set('verify_emails', $form_state->getValue('verify_emails'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
